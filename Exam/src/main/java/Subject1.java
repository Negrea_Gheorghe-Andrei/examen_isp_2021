public class Subject1 {
    //inheritance
    class L extends Base {
        private long t;
        M m;  //association

        public void f() {
        }
    }

    class Base {
        public float n;

        public void f(int g) {
        }
    }

    class X {
        //dependency
        public void i(L l) {
        }
    }

    class M {

    }

    class A {
        //composition
        M m = new M();

        public void metA() {
        }
    }

    class B {
        //aggregation
        M m;

        B(M m) {
            this.m = m;
        }

        public void metB() {
        }
    }
}
