import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Subject2 extends JFrame {
    JTextField textField1;
    JTextField textField2;
    JTextField textField3;
    JButton button;

    Subject2() {
        setTitle("Sum of two numbers");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(250, 250);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);
        int width = 80;
        int height = 20;

        textField1 = new JTextField();
        textField1.setBounds(70, 50, width, height);

        textField2 = new JTextField();
        textField2.setBounds(70, 80, width, height);

        textField3 = new JTextField();
        textField3.setBounds(70, 110, width, height);
        textField3.setEditable(false);

        button = new JButton("Enter");
        button.setBounds(70, 140, width, height);

        button.addActionListener(new Subject2.TreatButtonPress());

        add(textField1);
        add(textField2);
        add(textField3);
        add(button);
    }

    public static void main(String[] args) {
        new Subject2();
    }

    class TreatButtonPress implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            int nr1;
            int nr2;
            nr1 = Integer.parseInt(Subject2.this.textField1.getText());
            nr2 = Integer.parseInt(Subject2.this.textField2.getText());
            int sum;
            sum = nr1 + nr2;
            Subject2.this.textField3.setText(Integer.toString(sum));
            Subject2.this.textField1.setText("");
            Subject2.this.textField2.setText("");
        }
    }
}
