package test;

public class Test {
    public boolean test=true;

    public boolean getTest(){
        return this.test;
    }

    public void setTest(boolean test){
        this.test=test;
    }

    @Override
    public String toString(){
        return "Is this a test?\n" + getTest();
    }
}
